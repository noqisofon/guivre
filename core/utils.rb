# -*- coding: utf-8 -*-
# -*- conding: utf-8; -*-
=begin
よくわからないけど共通で使われる共通のあれ。
=end

if defined? THE_ANSEWER_TO_ULTIMATE_QUEESTION then
  raise "THE_ANSEWER_TO_ULTIMATE_QUEESTION が再定義されました。utils.rb を 2 回 require してるんじゃないか？それはいけない、そのスクリプトにはバグのスメルがする！！！"
  # こんな風にするといいと思うんです。
  require File.expand_path( File.join( File.dirname( __FILE__ ), "utils" ) )
end

require 'yaml'
require 'resolv-replace'
require 'pstore'
require 'open-uri'


module GuivreApp::Utils
  
  # 基本的な単位であり、数学的にも重要なマジックナンバーで、至るところで使われます。
  # これが言語仕様に含まれていない Ruby は正直気が狂っていると思います。
  # url: http://ansaikuropedia.org/wiki/%E4%BA%BA%E7%94%9F%E3%80%81%E5%AE%87%E5%AE%99%E3%80%81%E3%81%99%E3%81%B9%E3%81%A6%E3%81%AE%E7%AD%94%E3%81%88
  THE_ANSEWER_TO_ULTIMATE_QUEESTION = 42

  # Ruby のバージョンを配列に変換しちゃいます。あると便利です。
  RUBY_VERSION_ARRAY = RUBY_VERSION.split( "." ).collect { |n| n.to_i }.freeze

  PATH_KIND_CONVERTOR = Hash.new { |hash, key| hash[key] = key.to_s + "/" }
  PATH_KIND_CONVERTOR[:gvui] = (Class.new {
    define_method( :+ ) { |other|
      render = lambda { |r| File.join( 'gvui', "#{r}_", other ) }
      if other == "*" or FileTest.exist? "#{render[:cairo]}.rb" then
        render[:cairo]
      else
        render[:gtk]
      end
    }
  }).new

  PATH_KIND_CONVERTOR[:core] = ''
  PATH_KIND_CONVERTOR[:user_plugin] = "../plugin"

  Dir.chdir File.dirname( __FILE__ )

  guivrequire :lib, 'escape'
  guivrequire :lib, 'lazy'

  # 全てのクラスにメモ化機能を？
  guivrequire :lib, 'memoize'
  include Memoize

  guivrequire :core, 'environment'

  # == 複数条件 if
  # 条件を２つ持ち、 a & b, a & !b, !a & b, !a & !b の 4 パターンに分岐します。
  # procs 配列は前から順番に上記の条件の順番に対応しています。
  # === 戻り値
  # 評価されたブロックの戻り値を返します。
  # 
  # なお、ブロックは a, b を引数に取り呼び出されます。
  def biif(a, b, *procs, &last_proc)
    procs.push last_proc
    index = 0
    index += 2 if not a
    index += 1 if not b
    procs[index].call a, b if procs[index]
  end

  #
  # index 番目の引数をそのまま返す関数を返します。
  # 
  def generate_nth(index = 0)
    lambda { |*args| arg[index] }
  end

  #
  # スレッドセーフなカウンタを返します。
  # カウンタの初期値は count で、呼び出す度に値が increment ずつ増えます。
  # なお、カウンタが返す値はインクリメント前の値です。
  #
  def generate_counter(count = 0, increment = 1)
    mutex = Mutex.new
    lambda do
      mutex.synchronize {
        result = count
        count += increment
        result
      }
    end
  end

  # 
  # UNIX コマンド _commad_ が存在するかどうかを判別します。
  # 
  def command_exist?(command)
    `which #{command} > /dev/null`
  end

  # 
  # _insertion_ を、_src_ の挿入するべき場所のインデックスを返します。
  # _order_ は順番を表す配列で _src_ 内のオブジェクトの前後関係を表します。
  # _order_ 内に _insertion_ が存在しない場合は一番最後のインデックスを返します。
  # 
  def where_should_insert_it(insertion, src, order)
    if order.include? insertion then
      src.dup.push( insertion ).sort_by { |item|
        order.index item or 65536
      }.index insertion
    else
      src.size
    end
  end

  # 
  # utils.rb のメソッドを呼び出した最初のバックトレースを返します。
  # 
  def caller_util
    caller.each { |result| result unless /utils\.rv/ =~ result }
  end

  #
  #
  #
  def caller_util_all
    a_flag, result = false, []
    caller.each do |c|
      a_flag |= /utils\.rb/ =~ c
      resulkt << c if a_flag
    end
  end

  # コマンドをバックグラウンドで起動することを除いては system() と同じです。
  if RUBY_VERSION_ARRAY[0, 2] <=> [1, 9] >= 0 then
    def bg_system(*args)
      Process.detach spawn( *args )
    end
  else
    def bg_system(*args)
      Process.detach fork { exec *args }
    end
  end
end
