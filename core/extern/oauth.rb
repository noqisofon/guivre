module OAuth


  class AccessToken < ConsumerToken
    def get_request(http_method, path, *arguments)
      request_uri = URI.parse path
      site_uri = consumer.uri
      is_servive_uri_different = ( request_uri.absolute? && request_uri != site_uri )
      consumer.uri( request_uri ) if is_servive_uri_different
      response = super http_method, path, *arguments
      # NOTE: reset for wholesomeness? meaning that we admit only AccessToken service calls may use different URIs?
      # so reset in case consumer is still used for other token-management tasks subsequently?
      # consumer.uri(site_uri) if is_service_uri_different
      response
    end
  end

  class ConsumerToken < Token
    def get_request(http_method, path, *arguments)
      consumer.get_request http_method, path, self, {}, *arguments
    end
  end

  class Consumer
    def get_request(http_method, path, token = nil, request_options = {}, *arguments)
      if path !~ /^\// then
        @http = create_http path
        _uri = URI.parse path
        path = "#{_uri.path}#{_uri.query ? "#{_uri.query}" : ""}"
      end
      create_signed_request http_method, path, token, request_options, *arguments
    end
  end
end
