# -*- coding: utf-8 -*-
=begin
logging 用のユーティリティ。
=end

require 'logger'

module GuivreApp::Utils
  @@logger = Logger.new( log_device )
  @@debug_avail_level = Logger::WARN

  @@logger.level = @@debug_avail_level

  # 
  # 一般メッセージを表示します。
  # 
  def notice(message)
    #log "notice", message if @@debug_avail_level >= Logger::INFO
    @@logger.info messsage
  end

  # 
  # 警告メッセージを表示します。
  # 
  def warn(message)
    #log "warn", message if @@debug_avail_level >= Logger::WARN
    @logger.warn message
  end

  # 
  # エラーメッセージを表示します。
  # 
  def error(message)
    #log "error", message if @@debug_avail_level >= Logger::ERROR
    @@logger.error message
  end

  # 
  # ファタルメッセージを表示します。
  # 
  def fatal(message)
    @@logger.fatal message
    abort if @@debug_avail_level >= Logger::FATAL
  end

  # 
  # エラーレベルを設定します。
  # 
  def set_error_level(level = :error)
    case level
    when :notice, :info
      @@logger.level = @@debug_avail_level = Logger::INFO
    when :warn
      @@logger.level = @@debug_avail_level = Logger::WARN
    when :error
      @@logger.level = @@debug_avail_level = Logger::ERROR
    when :fatal
      @@logger.level = @@debug_avail_level = Logger::FATAL
    else
      @@logger.level = @@debug_avail_level = level
    end
  end


  private
  # 
  # エラーログに記録します。
  # 内部処理用。外部からは呼び出さないこと。
  # 
  # def log(prefix, object)
  #   begin
  #     message = "#{prefix}: #{caller_util}: #{object}"
  #     message += "\nfrom " + object.backtrace.join( "\nfrom " ) if object.is_a? Exception
      
  #     unless @@daemon then
  #       if message.is_a? Exception then
  #         #__write_stderr message.to_s
  #         @@log.
  #         __write_stderr message.backtrace.join "\n"
  #       else
  #         __write_stderr message
  #       end

  #       if log_file then
  #         FileUtils.mkdir_p File.expand_path( File.dirname "#{log_file}_" )
  #         File.open( File.expand_path( "#{log_file}#{Time.now.strftime( '%Y-%m-%d' )}.log", "a" ) ) do |bothput|
  #           bothput.write "#{Time.now.to_s}: #{message}\n"
  #         end
  #       end
  #     end
  #   rescue Exception => exception
  #     __write_stderr "critical!: #{caller( 0 )}: #{exception.to_s}"
  #   end
  # end

  # #
  # #
  # #
  # FOLLOW_DIR = File.expand_path '..'
  # #
  # #
  # #
  # def __write_stderr(message)
  #    $stderr.puts message.gsub( FOLLOW_DIR, "#{GUIVRE_DIR}" )
  # end

  # 
  # ログファイルの設定を行います。
  # 
  def log_file(filename = nil)
    if filename then
      @@log_file = filename
    end
    @@log_file or nil
  end

  # 
  # ログデバイスを返します。
  # 
  def log_device(filename = nil)
    if filename then
      FileUtils.mkdir_p File.expand_path( File.dirname( "#{filename}_"  ) )
      log_bothput = File.open( File.expand_path( "#{filename}#{Time.now.strftime( '%Y-%m-%d' )}.log" ), "a" )
      @@log_device = Logger.LogDevice.new( log_bothput )
    else
      @@log_device = Logger.LogDevice.new( $stderr ) unless @@log_device
    end
    @@log_device or nil
  end
end
