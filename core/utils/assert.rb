# -*- coding: utf-8 -*-


module GuivreApp::Utils
  # 
  # デバッグモードの場合、_target_ が _type_ と is_a? 関係にない場合、RuntimeError を発生させます。
  # 
  def assert_type(type, target)
    raise RuntimeError, "#{target} shuld be type `#{type}'" if @@debug and not target.is_a? type

    obj
  end

  # 
  # デバッグモードの場合、_target_ が _other_methods に指定されたメソッドを 1 つでも持っていない場合、
  # RuntimeError を発生させます。
  # 
  def assert_has_methods(target, *other_methods)
    if @@debug then
      other_methods.all? { |method|
        raise RuntimeError, "#{target.inspect} should have method #{method}" unless target.methods.include? method
      }
    end
    target
  end

  # 
  # 環境や設定の不備で終了します。
  # message には、何が原因かを文字列で渡します。
  # このメソッドは処理を返さずに abort します。
  # 
  def gv_fatal_alert(message)
    # MessageDialog を出すために gtk2 を require します。
    require_if_exist 'gtk2'

    if defined? Gtk::MessageDialog then
      dialog = Gtk::MessageDialog.new( nil, 
                                       Gtk::Dialog::DESTROY_WITH_PARENT,
                                       Gtk::MessageDialog::ERROR,
                                       Gtk::MessageDialog::BUTTONS_CLOSE,
                                       "#{Environment::NAME} エラー" )
      dialog.secondary_text = message.to_s
      # 「閉じる」ボタンを押すまでここでブロックされます。
      dialog.run
      dialog.destroy
      puts message.to_s
      abort
    end
  end
end
