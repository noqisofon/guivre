# -*- coding: utf-8 -*-


module GuivreApp::Utils
  # 
  # ファイルの内容を文字列で読み込みます。
  # 
  def file_get_contents(filename)
    open( filename, "r" ) { |output| output.read }
  end

  # 
  # 文字列を指定されたファイルに書き込みます。
  # 
  def file_put_contents(filename, body)
    File.open( filename, "w" ) do |input|
      input.write body
    end
    body
  end

  # 
  # ファイル _filename_ の内容を読み込んでオブジェクトを返します。
  # 
  def object_get_contents(filename)
    File.open( filename, "r" ) { |output| Marshal.load output }
  end

  # 
  # オブジェクト _body_ を _filename_ に書き込みます。
  # _body_ は Marshalize できるものでなければなりません。
  # 
  def object_put_contents(filename, body)
    File.open( filename, "w" ) { |input| Marshal.dump body, input }
  end

  # 
  # YAML ファイルを読み込みます。
  # 存在しない場合は空のハッシュを返します。
  # \param _file_ IO オブジェクトかファイル名。
  # 
  def conf_load(file)
    if file.is_a? IO then
      YAML.load file.read
    elsif FileTest.exist? File.expand_path( file ) then
      YAML.load file_get_contents( file )
    else
      {}
    end
  end
end
