# -*- coding: utf-8 -*-
# 
# Config
#
# guivre 用の設定を定義しています。
# 
module GuivreApp::Config
  #
  #
  #
  AUTO_TAG = false
  #
  #
  #
  NAME = "guivre"
  #
  #
  #
  ACRO = NAME
  #
  #
  #
  VERSION = [ Version::MAJOR, Version::MINOR, Version::REVISION ]
  #
  #
  #
  REVISION = Version::REVISION
  #
  # 
  #
  CONF_ROOT = "~/.#{NAME}"
  #
  #
  #
  LOG_DIR = "#{CONF_ROOT}/log"
  #
  #
  #
  TMP_DIR = "#{CONF_ROOT}/tmp"
  #
  #
  #
  PID_FILE = "/tmp/#{NAME}.pid"
  #
  #
  #
  TWITTER_CONSUMER_SECRET = "YM01qSkuRcXAxKWcthNOw"
  #
  #
  #
  TWITWER_CONSUMER_KEY = "yHveugMdnCbx2oZXOhRPjetePRblEkNKbg9p2EXbI"
  #
  # 再起動後に、前回取得したポストを取得しないようにします。
  #
  NEVER_RETRIEVE_OVERLAPPED_MUMBLE = false
end
