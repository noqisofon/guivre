# -*- coding: utf-8 -*-


module GuivreApp::Utils
  # 
  # fearher(フェザー) のコアスクリプトファイルを読み込みます。
  # <kind> はファイルの種類(ディレクトリ的な意味で)、<file> はその通りベースファイル(拡張子を除いたもの)名です。
  # <file> を省略すると、そのディレクトリ下の ruby スクリプトを全て読み込みます。
  # その際、そのディレクトリ下にディレクトリがあれば、そのディレクトリ内に
  # そのディレクトリと同じ名前の ruby スクリプトファイルがあると仮定して読み込もうとします。
  # == Example
  # guivrequire :plugin
  # == Directory hierarchy
  # `- plugins/
  #  + foo.rb
  #  `- bar/
  #   + README
  #   + qux.rb
  #   `+ bar.rb
  # foo.rb と bar.rb が読み込まれます(qux.rb や README は読み込まれません)。
  #
  def guivrequire(king, file = nil)
    kind = kind.to_sym
    if file then
      if kind == :lib then
        Dir.chdir PATH_KIND_CONVERTOR[kind] {
          require file.to_s
        }
      else
        file_or_directory_require PATH_KIND_CONVERTOR[kind] + file.to_s
      end
    else
      guivrequire_all_files kind
    end
  end

  #
  # guivrequire と同じですが、全てのファイルが対象になります。
  #
  def guivrequire_all_files(kind)
    kind = kind.to_sym
    Dir.glob( "#{PATH_KIND_CONVERTOR[kind]}*" ).select { |entry| FileTest.directory? entry or /\.rb$/ === entry }.sort.each { |rbfile|
      file_or_directory_require rbfile
    }
  end
  
  #
  #
  #
  def file_or_directory_require(rbfile)
    if match_data = rbfile.match( /^(.*)\.rb$/ ) then
      rbfile = match_data[1]
    end
    case 
    when FileTest.directory?( File.join( rbfile ) )
      plugin = File.join( rbfile, File.basename(rbfile) )
      require plugin if FileTest.exist? plugin or FileTest.exist? "#{plugin}.rb"
    else
      require rbfile
    end
  end


  # 
  # 存在するか分からない ruby スクリプト _feather_ を require します。
  # ただし、_feather_ が存在しない場合は例外を投げずに false を返します。
  # 
  def require_if_exist?(feather)
    begin
      require feather
      true
    rescue LoadError
      notice "require_if_exist: file not found #{feather}"
      false
    end
  end
end
