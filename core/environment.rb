# -*- coding: utf-8 -*-
# 
# Environment
#
# guivre の環境変数を定義しています。
# 
guivrequire :core, 'config'

module GuivreApp::Environment
  # 
  # AutoTag が有効かどうかを表します。
  # 
  AUTO_TAG = Config::AUTO_TAG
  #
  # このアプリケーションの名前です。
  #
  NAME = Config::NAME
  # 
  # 名前の略称です。
  # 
  ACRO = Config::ACRO
  # 
  # 一時ディレクトリのパスです。
  # 
  TMPDIR = Config::TMP_DIR
  # 
  # キャッシュディレクトリのパスです。
  # 
  CACHE = Config::CACHE
  #
  #
  #
  LOG_DIR = Config::LOG_DIR
  # 
  # pid ファイルです。
  # 
  PID_FILE = Config::PID_FILE
  # 
  # 下の二行はコードリーディング莫迦には見えません。
  # 
  TWITTER_CONSUMER_KEY = Config::TWITTER_CONSUMER_KEY
  TWITTER_CONSUMER_SECRET = Config::TWITTER_CONSUMER_SECRET
  #
  # 再起動後に、前回取得したポストを取得しないようにするかどうかを表します。
  #
  NEVER_RETRIEVE_OVERLAPPED_MUMBLE = Config::NEVER_RETRIEVE_OVERLAPPED_MUMBLE
end
