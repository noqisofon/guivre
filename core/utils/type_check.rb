# -*- coding: utf-8 -*-


module GuivreApp::Utils
  # 
  # 引数のチェックを全てパスした場合のみ、ブロックを実行します。
  # チェックに引っかかった項目があれば warn を出力してブロックは実行せずに nil を返します。
  # チェックは assoc できる配列か、Hash で定義します。
  # 
  # type_check( value => nil,                  # チェックしません(常にパス)。
  #             value => Module,               # その型と is_a? 関係ならパス。
  #             value => [ :method. *args ],   # value.method *args が真を返せばパス。
  #             value => lambda { |x| ... } )  # x に value を渡して実行し、真が返ってくればパス。
  # 
  # チェックを全てパスしたかどうかを真偽値で返します。
  # ブロックが指定されていれば、それを実行して結果を返します。
  # メモ:
  #      何れかのタイプに一致するチェックを定義するには tcor を使います。
  #  type_check object => tcor(Array, Hash)
  # 
  def type_check(args, &proc)
    check_function = lambda { |val, check|
      if not check then
        true
      elsif check.respond_to? :call then
        check.call val
      elsif check.is_a? Array then
        val.__send__ *check
      elsif check.is_a? Symbol then
        val.respond_to? check
      elsif check.is_a? Class then
        val.is_a? check
      end
    }
    error = args.find { |arg| not check_function.call *arg }

    if error then
      warn "argument error: #{error[0].inspect} is not passed #{error[1].inspect}"
      warn "in #{caller_util}"
    else
      if proc then
        yield
      else
        true
      end
    end
  end  # end type_check

  # 
  # _types_ のうちいずれかと is_a? 関係なら true を返す Proc オブジェクトを返します。
  # 
  def type_check_or(*types)
    lambda do |v|
      types.any? { |c| v.is_a? c }
    end
  end

  # 
  # type_check とほとんど同じですが、チェックをパスしなかった場合に abort します。
  # type_check の戻り値を返します。
  # 
  def type_strict(args, &proc)
    result = type_check( args, &proc )
    raise ArgumentError.new unless result
    result
  end

  #  
  # block の評価結果がチェックをパスしなかった場合に abort します。
  # 
  def result_strict(must, &block)
    result = yield
    type_strict result => must
    result
  end

  # 
  # type_check で型をチェックしてからブロックを評価する無名関数を生成して返します。
  # 
  def type_check_lambda(*args, &proc)
    lambda { |*xs|
      if proc.arity >= 0 then
        raise ArgumentError.new "wrong number of arguments (#{xs.size} for #{proc.arity})" unless proc.arity == xs.size
      elsif -( proc.arity + 1 ) > xs.size then
        raise ArgumentError.new "wrong number of arguments (#{xs.size} for #{proc.arity})"
      end
      yield *xs if type_check xs.slice( 0, args.size ).zip( args )
    }
  end
end
