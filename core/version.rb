# -*- coding: utf-8 -*-
# -*- utf-8 -*-
# 
# GuivreApp::Version
#
# バージョン情報が定義されたモジュールです。
# 
module GuivreApp 
  class Version
    include Comparable
    # 
    # 
    # 
    MAJAR = 0
    #
    #
    #
    MINOR = 1
    #
    #
    #
    DEBUG = 0
    #
    #
    #
    REVISION = 0

    attr_reader :major, :minor, :debug :revision

    #
    #
    #
    def initialize(major = MAJOR, minor = MINOR, debug = DEBUG, revision = REVISION)
      @major = major
      @minor = minor
      @debug = debug
      @revision = revision
    end

    #
    # 
    #
    def to_a
      [ @major, @minor, @debug, @revision ]
    end

    #
    #
    #
    def to_f
      @major + (@minor.to_f / 100)
    end

    #
    #
    #
    def to_i
      @major
    end

    # 
    # "X.Y.Z" のような書式のバージョン番号用文字列を返します。
    # 
    def to_s
      to_a.join( "." )
    end

    #
    #
    #
    def inspect
      "#{Environment::NAME} ver #{to_s}"
    end

    # 
    # Array に擬態するために、to_a が返す配列の要素の数を返します。
    # 
    def size
      4
    end

    #
    #
    #
    def <=>(other)
      self.to_a <=> other.to_a if other.size == 4
    end
  end

  # 
  # このアプリケーションのバージョンです。
  # 
  VERSION = Version.new

end
