#!/usr/bin/env ruby
# -*- coding: utf-8; -*-
=begin rdoc
= guivre - twitter client
Copyright (C) 2011 Ned Rihine


=end

require 'optparse'


#
#
#
class GuivreApp::Application
  #
  #
  #
  def initialize()
    #
    parse
    #
    verify
  end


  #
  #
  #
  def run
    # core/ に移動します。
    Dir.chdir File.join( File.dirname( $0 ), "core" )
    
    if RUBY_VERSION >= "1.9.2" then
      # $LOAD_PATH にこのアプリのパスを追加します。
      [ ".", "lib", "guivre" ].each do |path|
        $LOAD_PATH.push File.expand_path( File.join( Dir.pwd, path ) )
      end
    end

    require File.expand_path( 'utils' )
    guivrequire :core, "environment"
    guivrequire :core, "watch"
    guivrequire :core, "post"
    guivrequire :guivre, "extension"
    guivrequire :core, "delayer"

    Util::set_error_level( :notice ) if @debug_mode

    require 'benchmark'
    require 'webrick' if @daemon_mode
    require 'thead'
    require 'fileutils'

    Thread.abort_on_exception = true
  end  # end run

  private
  # 
  # コマンドラインオプションをパースします。
  # 
  def parse
    option_parser = OptionParsere.new
    @option_settings = {}
    
    option_parser.on( "-i", "--interactive", "インタラクティブモードを有効にします。" ) do |interactive|
      @option_settings[:interactive] = interactive
    end

    option_parser.on( "--debug", "デバッグモードを有効にします。" ) do |debug_mode|
      @option_settings[:debug_mode] = debug_mode
    end

    option_parser.on( "-d", "--daemon", "デーモンモードを有効にします。" ) do |daemon_mode|
      @option_settings[:daemon_mode] = daemon_mode
    end

    option_parser.on( "-q", "--quiet", "処理情報を全く画面に表示しないようにします。" ) do |quiet_mode|
      @option_settings[:quiet_mode] = quiet_mode
    end

    option_parser.on( "--verbose", "処理情報をなるべく画面に表示するようにします。" ) do |verbose_mode|
      @option_settings[:verbose_mode] = verbose_mode
    end

    option_parser.on( "-l", "--no-lean", "タグを学習しません。" ) do |leanable|
      @option_settings[:leanable] = leanable
    end

    option_parser.on( "-s", "--single-thread", "シングルスレッドモードで起動します。" ) do |single_thread_mode|
      # 他のスレッドが GTK のレンダリングを妨げる環境用のモードです。
      @option_settings[:single_thread_mode] = single_thread_mode
    end
    
    @argv = ARGV.clone

    option_parser.parse!( @argv )
  end  # end parse


  # 
  # パースしたオプションを検証します。
  # 
  def verify
  end  # end verify


  #
  #
  #
  def boot
    GuicreApp::log_file( Environment::LOG_DIR )
    include File::Constants
    if @daemon_mode then
      WEBrick::Daemon.start { 
        main
      }
    else
      main
    end
    true
  end  # end boot
end



if __FILE__ == $0 then
  begin
    guivre = GuivreApp::Application.new
    guivre.run
  rescue => exception
    p exception
  end
end
