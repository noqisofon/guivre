# -*- coding: utf-8 -*-

require 'thread'
require 'monitor'

module GuivreApp::Utils

  @@atomic = Monitor.new

  # 
  # プロセス ID _pid_ が存在するかどうかを判別します。
  # 
  def pid_exist?(pid)
    if FileTest.exist? "/proc" then
      FileTest.exist? "/proc/#{pid}"
    else
      begin
        Process.kill 0, pid.to_i
      rescue Errno::ESRCH
        false
      else
        true
      end
    end
  end

  # 
  # カレントスレッドがメインスレッドかどうかを返します。
  # 
  def main_thread?
    Thread.main == Thread.current
  end

  # 
  # メインスレッド以外で呼び出されたら ThreadError を投げます。
  # 
  def main_thread_only 
    raise ThreadError.new "The method can calls only main thread. but called by another thread." unless main_thread?
  end

  # 
  # メインスレッド以外で呼び出されたら ThreadError を投げます。
  # 
  def not_main_thread
    raise ThreadError.new "The method can calls only another thread. but called by main thread." if main_thread?
  end

  # 
  # 共通の Mutex で処理っを保護して実行します。
  # atomic ブロックで囲まれたコードは、別々のスレッドで同時に実行されません。
  # 
  def atomic
    if main_thread? then
      # raise "Atomic Mutex don't have to block main thread"
    end
    @@atomic.synchronize { yield }
  end
end
