# -*- encoding: utf-8; -*-
# 
# 
# 
guivrequire :core, 'twitter_api'
guicrequire :core, 'message'
guicrequire :core, 'message_convertors'

require 'rexml/document'
require_if_exist 'rubygems'
require_if_exist 'httpclient'


class GuivreApp::Twitter < GuivreApp::TwitterAPI
  PROG_NAME = Environment::NAME


  #
  #
  #
  def update(message)
    text = self.convert_message message
    return nil if not text
    reply_to = message[:reply_to]
    receiver = message[:receiver] or get_receiver( message )
    data = { :status => text }
    data[:in_reply_to_user_id] = User.generate( receiver )[:id].to_s if receiver
    data[:in_reply_to_status_id] = Message.generate( reply_to )[:id].to_s if reply_to

    post_with_auth "/statuses/update.#{FORMAT}", data
  end

  # 
  # DM を送ります。
  # \param message 送信する本文です。
  # \keyword :user 宛先。User オブジェクトを渡します。
  # 
  def send_direct_message(message)
    text = self.convert_message message
    post_with_auth( "/direct_messages/new.#{FORMAT}",
                    :text => text, :user => User.generate( message[:user][:id].to_s ))
  end

  #
  #
  #
  def image_uploadable?
    defined? HTTPClient
  end

  #
  #
  #
  def upload_image(image, message)
    if not self.image_uploadable? then
      notice "twitter: HTTPClient is not installed. failed to image upload"
      return nil
    end

    result = nil
    twit_pic = HTTPClient.new
    notice "twitter: twitpic uploading image"
    verify_credentials = "http://api.twitter.com/1/account/verify_credentials.json"
    ac_token = access_token
    request = ac_token.get_request :get, verify_credentials
    auth = request['Authorization']
    response = twit_pic.post_content( "http://api.twitpic.com/2/upload.json",
                                      { :key => "030381b5b137acbb428c3661eb797d4e",
                                        :message => message,
                                        :media => image },
                                      { 'X-Auth-Servise-Provider' => verify_credentials,
                                      'X-Verify-Credentials-Authorization' => auth + ",realm=\"http://api.twitter.com\"" } )
    notice "twitter: twitpic response: #{response.inspect}"
    json = JSON.parse response
    if json then
      result = json['url']
      if result then
        notice "twitter: twitpic upload success. url: #{result}"
        result
      else
        warn "twitter: twitpic upload failed."
      end
    end
  end

  #
  #
  #
  def get_image(message)
    image = message[:image]
    if not image.url and image.resource then
      image.url = self.upload_image image.resource, message[:message]
      if image.url == nil then
        error_image_path = "/tmp/#{Time.now.strftime( Environment.ACRO + 'errorimage-%Y%m%d-%H%M%S' )}"
        FileUtils.copy image.path, error_image_path
      end
    end
    image.url
  end

  #
  #
  #
  def convert_message(message)
    result = [ message[:message] ]
    if message[:tags].is_a? Array then
      tags = message[:tags].select { |tag| not message[:message].include?( tag.to_s ) }.map{ |item| "##{item}" }
      notice tags.inspect
      result.concat( tags ) if tags
    end
    receiver = get_receiver message
    if receiver and not message[:message].include? "@#{receiver[:idname]}" then
      result = [ "@#{receiver[:idname]}", *result ]
    end
    if message[:image] then
      image = get_image message
      if image then
        result << image
      else
        return nil
      end
    end
    if message[:retweet] then
      result << "RT" << "@#{message[:retweet][:user][:idname]}:" << message[:retweet].to_s
    end
    text = result.join( " " )
    if UserConfig[:shrinkurl_always] or text.strsize > 140 then
      text = MessageConvertors.shrink_url_all text 
    end
    text.shrink 140, Uri.regexp( [ 'http', 'https' ] ) if text
  end

  #
  #
  #
  def get_receiver(message)
    if message[:receiver] then
      User.generate message[:receiver]
    elsif /@([a-zA-Z0-9_]+)/ === message[:message] then
    User.find_by_idname $1
    elsif message[:reply_to] then
      message[:reply_to][:user] rescue nil
    end
  end
end


guicrequire :extern, 'oauth'
