# -*- encoding: utf-8; -*-
# 
# 
# 
require 'net/http'
require 'thread'
require 'base64'
require 'io/wait'

guivrequire :lib, 'oath'
guivrequire :lib, 'reserver'
guivrequire :core, 'environment'
guivrequire :core, 'plugin'
guivrequire :core, 'config_loader'

Net::HTTP.version_1_2

=begin
class TwitterAPI
=end
class GuivreApp::TwitterAPI < Mutex
  #
  #
  #
  HOST = "api.twitter.com".freeze
  #
  #
  #
  BASE_PATH = "http://#{HOST}/1".freeze
  #
  #
  #
  OPEN_TIMEOUT = 10
  #
  #
  #
  READ_TIMEOUT = 20
  #
  #
  #
  FORMAT = "json".freeze
  #
  #
  #
  API_MAX = 150
  #
  #
  #
  API_RESET_INTERVAL = 3600
  #
  #
  #
  OAUTH_VERSION = "1.0".freeze
  #
  #
  #
  DEFAULT_API_ARGUMENT = { :include_entities => 1 }.freeze
  #
  #
  #
  CACHE_EXPIRE = 60 * 60 * 24 * 2
  #
  #
  #
  EXCLUDE_OPTIONS = [ :cache ].freeze

  include ConfigLoader

  #
  #
  #
  @@failed_lock = Monitor.new
  #
  #
  #
  @@last_success = nil
  #
  #
  #
  @@test_mode = false
  #
  #
  #
  @@next_test_response = '200'

  #
  #
  #
  def initialize(a_token, a_secret, &fail_trap)
    super
    @a_token, @a_secret = a_token, a_secret
    @get_mutex = Mutex.new
    @fail_trap = fail_trap? fail_trap: nil
  end

  #
  #
  #
  def self.test_mode
    @@test_mode = true
  end

  #
  #
  #
  def self.next_test_response=(ntr)
    next_test_response = ntr
  end

  #
  #
  #
  def self.garbage_collect
    File.delete *Dir.glob( "#{Environment.CACHE}**#{File::Separator}*" ).select( &method( :is_toooldd ) ) rescue nil
  end

  #
  #
  #
  def self.is_tooold(file)
    Time.now - File.mtime( file ) > CACHE_EXPIRE
  end

  SerialThread.new { garbage_collect }

  #
  #
  #
  def consumer
    OAuth::Consumer.new( Environment::TWITTER_CONSUMER_KEY,
                         Environment::TWITTER_CONSUMER_SECRET,
                         :site => "http://twitter.com" )
  end

  #
  #
  #
  def access_token
    OAuth::AccessToken.new consumer, @a_token, @a_secret
  end

  #
  #
  #
  def request_oauth_token
    consumer.get_request_token
  end

  #
  #
  #
  def api_remain(response = nil)
    if response and response['X-RateLimit-Reset'] then
      time = Time.at( response['X-RateLimit-Reset'].to_i )
      @api_remain = [ response['X-RateLimit-Limit'].to_i,
                      response['X-RateLimit-Remaining'].to_i,
                      time
                    ]
      Reserver.new time { Plugin.call :before_exit_api_section }
    end
    return *@api_remain
  end

  #
  #
  #
  def ip_api_remain(response = nil)
    if response and response['X-RateLimit-Reset'] then
      @ip_api_remain = [ response['X-RateLimit-Limit'].to_i,
                         response['X-RateLimit-Remaining'].to_i,
                         Time.at( response['X-RateLimit-Reset'].to_i )
                       ]
    end
    return *@ip_api_remain
  end

  #
  # 規制されていたら真を返します。
  #
  def rate_limiting?
    limit, remain, rest = api_remain
    remain and reset and remain <= 0 and Time.new <= reset
  end
  
  #
  #
  #
  def user
    nil
  end

  #
  #
  #
  def connection(host = HOST)
    http = Net::HTTP.new host
    http.open_timeout = OPEN_TIMEOUT
    http.read_timeout = READ_TIMEOUT

    return http
  end

  #
  #
  #
  def ip_limit
    if @@ip_limit_reset then
      Time.now <= @@ip_limit_reset
    end
  end

  #
  #
  #
  def get_opts(options)
    result = {}
    result[:read] = {}
    options.each_pair do |k, v|
      case k
      when 'Cache'
        result[:cache] = v
      when 'Host'
        result[:host] = v
      else
        result[:head][k] = v
      end
    end
    result
  end

  #
  #
  #
  def cacheing(path, body)
    cache_path = File.expand_path( File.join( Environment::CACHE, path ) )
    FileUtils.mkdir_p File.dirname( cache_path )
    FileUtils.rm_rf cache_path if FileTest.exist? cache_path and not FileTest.file? cache_path
  rescue => err
    warn "cache write faided"
    warn err
  end

  #
  #
  #
  def cache_clear(path)
    cache_path = File.expand_path( File.join( Environment::CACHE, path ) )
    if FileTest.file? cache_path then
      return (Class.new {
                define_method( :body ) { file_get_contents( cache_path ) }
                define_method( :code ) { '200' }
              }).new
    end
  rescue
    warn "cache read failed"
    warn e
    nil
  end

  #
  #
  #
  def get(path, raw_options = {})
    return get_with_auth( path, raw_options ) if ip_limit
    options = get_opts raw_options
    if options[:cache] then
      cache = get_cache path
      return cache if cache
    end
    response = nil
    http = nil
    begin
      http = self.connection( options[:host] || HOST )
      http.start
      response = http.get path, options[:head]
      if response.is_a? Net::HTTPResponse and response.code == '200' and options.has_key? :cache then
        cacheing( path, response.body )
      end
    rescue Exception => evar
      response = evar
    ensure
      begin
        http.finish if http.active?
      rescue Exception => evar
        Log.warn( "TwitterAPI.get:finish" ) {
          "#{ever.inspect}"
        }
      end
      notice "#{path} => #{response}"
      if response.is_a? Net::HTTPResponse then
        limit, remain, reset = ip_api_remain( response )
        Plugin.call :ip_api_remain, remain, reset
        if response.code == '400' or response.code == '401' or response.code == '403' then
          if $debug and response.code != '400' then
            Plugin.call :update, nil, [ Message.new( :message => "Request protected account without OAuth.\n#{path}\n#{options.inspect}", :system => true ) ]
          end
          @@ip_limit_reset = reset
          return get_with_auth path, raw_options
        end
        response
      end
    end
  end

  #
  #
  #
  def get_with_auth(path, raw_options = {})
    query_with_auth :get, path, raw_options
  end

  #
  #
  #
  def post_with_auth(path, data = {})
    query_with_auth :post, path, data
  end

  #
  #
  #
  def delete_with_auth(path, raw_options = {})
    query_with_auth :delete, path, raw_options
  end

  #
  #
  #
  def userstream
    access_token.method( :get ).call( "httpss://userstream.twitter.com/2/user.json",
                                      'Host' => 'userstream.twitter.com', 
                                      'User-Agent' => "#{Environment::NAME}/#{Environment.VERSION}" ) do |respose|
      response.read_body &Proc.new
    end
  rescue Exception => err
    warn err
  end

  #
  #
  #
  def filter_stream(params = {})
    callback = Proc.new
    buf = ""
    access_token.method( :get ).call( "https://stream.twitter.com/1/statuses/filter.#{FORMAT}#{get_args(params)}",
                                      'Host' => 'stream.twitter.com',
                                      'User-Agent' => "#{Environment::NAME}/#{Environment.VERSION}" ) do |response|
      response.read_body do |chunk|
        if chunk.split( //u )[-1] == "\n" then
          callbak.call buf + chunk
          buf.clear
        else
          buf << chunk
        end
      end
    end
  rescue Exception => err
    warn err
  end

  #
  #
  #
  def query_with_auth(method, path, raw_options = {})
    serial = query_serial_number
    options = get_opts raw_options
    if options[:cache] and options[:cache] != :keep then
      cache = get_cache path
      return cache if cache
    end
    access_token = OAuth::AccessToken.new consumer, @a_token, @a_secret
    response = nil
    start_time = Time.new.freeze
    begin
      Plugin.call( :query_start,
                   :serial => serial,
                   :method => method,
                   :path => path,
                   :options => options,
                   :start_time => start_time )
      notice "request #{method} #{path}"
      begin
        response = access_token.method( method ).call( File.join( BASE_PATH, path ), options[:head] )
      rescue Exception => err
        response = err
      end
      notice "#{method} #{path} => #{response} (#{(Time.new - start_time).to_f}s)"
      begin
        limit, remain, reset = self.api_remain response
        Plugin.call :api_remain, remain, reset
      rescue => e
      end
      if response.is_a? Net::HTTPResponse then
        if response.code == '200' then
          cacheing path, response.body if options.has_key? :cache
        elsif response.code == '401' then
          begin
            return response if JSON.parse( response.body )["error"] == "Not authorized"
          rescue JSON::ParserError
          end
          if @fail_trap then
            last_success = @@last_success
            @@failed_lock.synchronize {
              @@last_success = @fail_trap.call if @@last_success == last_success
              @a_token, @a_secret, callback = *@@last_success
              callback.call if callback
              response = self.query_with_auth method, path, raw_options
            }
          end
        end
      end
      if response then
        response
      elsif options[:cache] == :keep then
        response = get_cache path
      end
    end
  ensure
    Plugin.call( :query_end,
                 :serial => serial,
                 :method => method,
                 :path => path,
                 :options => options,
                 :start_time => start_time,
                 :end_time => Time.new.freeze,
                 :response => response )
  end

  define_method :query_serial_number, &gen_number

  #
  #
  #
  def post(path, data, head)
    response = nil
    http = nil
    begin
      notice "post: try #{path}(#{data.inspect})"
      response = request 'POST', File.join( BASE_PATH, path ), data, head
    rescue Exception => err
      response = err
    end
    notice "#{path} => #{response}(#{(defined? response.body) and response.body})"
    response
  end

  #
  #
  #
  def option_since(since)
    since.httpdate =~ /^(.*?), (\S+) (\S+) (\S+) (\S+) (\S+)/
    "#{$1}%2C#{$2}+#{$3}+#{$4}+#{$5}+#{$6}"
  end

  #
  #
  #
  def public_timeline(since = nil)
    path = "/statuses/public_timeline.#{FORMAT}"
    path += "?since_id=#{since}" if since
    head = { 'Host' => HOST }
    get path, head
  end

  #
  #
  #
  def user_timeline(args = {})
    path = "/sutatuses/user_timeline.#{FORMAT}#{get_args( args.merge( DEFAULT_API_ARGUMENT ) )}"
    head = { 'Host' => HOST }
    if User.find_by_id( args[:user_id])[:protected] rescue nil then
      get_with_auth path, head
    else
      get path, head
    end
  end

  #
  #
  #
  def friends_timeline(args = {})
    path = "/sutatuses/home_timeline.#{FORMAT}#{get_args( args.merge( DEFAULT_API_ARGUMENT ) )}"
    head = { 'Host' => HOST }
    get_with_auth path, head
  end

  #
  #
  #
  def replies(args = {})
    path = "/sutatuses/mentions.#{FORMAT}#{get_args( args.merge( DEFAULT_API_ARGUMENT ) )}"
    head = { 'Host' => HOST }
    get_with_auth path, head
  end

  #
  #
  #
  def search(args = {})
    path = "/search.#{FORMAT}#{get_args( args.merge( DEFAULT_API_ARGUMENT ) )}"
    head = { 'Host' => "search.twitter.com" }
    get path, head
  end

  #
  #
  #
  def trends(args = nil)
    get "/trends.#{FORMAT}"
  end

  #
  #
  #
  def retweeted_to_me(args = {})
    path = "/sutatuses/retweets_of_me.#{FORMAT}#{get_args( args.merge( DEFAULT_API_ARGUMENT ) )}"
    head = { 'Host' => HOST }
    get_with_auth path, head
  end

  #
  #
  #
  def friendships(args = {})
    path = "/friendships/show.#{FORMAT}#{get_args( args )}"
    head = { 'Host' => HOST }
    get_with_auth path, head
  end

  #
  #
  #
  def frends(args = {})
    path = "/sutatuses/friends.#{FORMAT}"
    if args[:cache] == :keep and args[:cursor] == -1 then
      FileUtils.rm_rf Dir.glob( File.expand_path( File.join( Environment::CACHE, path ) ) + "*" )
    end
    get_with_auth "#{path}#{get_args( args )}", head( args )
  end

  #
  #
  #
  def followers(args = {})
    path = "/sutatuses/followers.#{FORMAT}"
    if args[:cache] == :keep and args[:cursor] == -1 then
      FileUtils.rm_rf Dir.glob( File.expand_path( File.join( Environment::CACHE, path ) ) + "*" )
    end
    get_with_auth "#{path}#{get_args( args )}", head( args )
  end

  #
  #
  #
  def friends_id(args = {})
    get_with_auth "/friends/ids.#{FORMAT}#{get_args( args )}", head( args )
  end

  #
  #
  #
  def followers_id(args = {})
    get_with_auth "/followers/ids.#{FORMAT}#{get_args( args )}", head( args )
  end

  #
  #
  #
  def direct_messages(args = {})
    args = DEFAULT_API_ARGUMENT.merge( :skip_status => true,
                                       :count => 200 ).merge( args )
    get_with_auth "/direct_messages.#{FORMAT}#{get_args( args )}", head( args )
  end

  #
  #
  #
  def sent_direct_message(args = {})
    args = DEFAULT_API_ARGUMENT.merge( :count => 200 ).merge( args )
    get_with_auth "/direct_messages/sent.#{FORMAT}#{get_args( args )}", head( args )
  end

  #
  #
  #
  def user_show(args)
    raise if args[:id].is_a? User
    get "/users/show.#{FORMAT}#{get_args(args)}", head( args )
  end

  #
  #
  #
  def user_lookup(args)
    if args[:id].empty? then
      nil
    elsif args[:id].include? ',' then
      get_with_auth "/users/lookup.#{FORMAT}?user_id=#{args[:id]}", 'Host' => HOST
    else
      user_show args
    end
  end

  #
  #
  #
  def status_show(args)
    id = args[:id]
    @last_id ||= Hash.new( 0 )
    type_strict id => Integer
    raise "id must than 1 but specified #{id.inspect}" if id <= 0
    @status_show_mutex ||= TimeLimitedStorage.new Integer, Mutex
    @status_show ||= TimeLimitedStorage.new Integer
    atomic { @status_show_mutex[id] ||= Mutex.new }.synchronize {
      return @status_show[id] if @status_show.has_key? id

      if @last_id[id] >= 10 then
        error "a lot of calls status_show/#{id}"
        error caller
        abort
      end
      @last_id[id] += 1

      
    }
  end
end
