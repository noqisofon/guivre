# -*- coding: utf-8 -*-


module GuivreApp::Utils
  # 
  # 文字列をエンティティデコードします。
  # 
  def entity_unescape(text)
    text.gsub /&(.{2,3})/ do |s|
      { 'gt' => '>', 'lt' => '<', 'amp' => '&' }[$1]
    end
  end

  #
  #
  #
  def mecab_wakach(text)
    IO.popen( "mecab -Owakati", "r+" ) do |mouth|
      mouth.write text
      mouth.close_write
      mouth.read
    end
  end

  momeize :mecab_wakach
end
