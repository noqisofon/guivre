# -*- coding: utf-8 -*-



module GuivreApp::Utils
  # Environment::CONF_ROOT 内のファイル名を取得します。
  #   GuivreApp::Utils::conf_root( *path )
  # は
  #   File::expand_path File.join( Environment::CONF_ROOT, *path )
  # と等価です。
  def conf_root(*path)
    File::expand_path File.join( Environment::CONF_ROOT, *path )
  end
end
