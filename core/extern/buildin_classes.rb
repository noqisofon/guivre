# -*- coding: utf-8 -*-


class Object
  #
  #
  #
  def self.defun(method_name, *args, &proc)
    define_method method_name, &type_check_lambda( *args, &proc )
  end

  # 
  # freeze できる時、true を返します。
  # 
  def freeze
    true
  end

  # 
  # freeze できる場合は freeze します。self を返します。
  # 
  def freeze_ifn
    freeze if freezable?
    self
  end

  # 
  # freeze サれていない同じ内容のオブジェクトを作成して返します。
  # 
  def melt
    frozen? ? dup: self
  end
end


class Numeric
  def freezable?
    false
  end
end


class Integer
  # 
  # ページあたり _one_page_contain_ 個の要素が入る場合に、self 番目の要素は何ページ目に来るかを返します。
  # 
  def page_of(one_page_contain)
    ( self.to_f / one_page_contain ).ceil
  end
end


class Float
  # 
  # 小数 _n_ 桁以前を削除します。
  # 
  def floor_at(n)
    nth_pow = 10 ** n
    ( self * nth_pow ).floor.to_f / nth_pow
  end

  def ceil_at(n)
    nth_pow = 10 ** n
    ( self * nth_pow ).ceil.to_f / nth_pow
  end

  def round_at(n)
    nth_pow = 10 ** n
    ( self * nth_pow ).round.to_f / nth_pow
  end

  # 
  # 百分率を返します(小数 _n_ 桁まで)。
  # 
  def percent(n = 0)end
end


module Enumerable
  # 
  # 複数の each の代わりになるメソッドを同時に使って繰り返します。
  # 引数には、メソッドをシンボルで渡すか、[ メソッド名, 引数... ] という配列を渡せます。
  # 例:
  #   ary = [ 1, 2, 3 ]
  #   ary.iterate( :each_with_index [ :inject, [ :foo ] ] ) { |a, x| a + x } # => [:foo, 1, 0, 2, 1, 3, 2]
  # 
  def iterate(*methods)
    methods.inject( self ) { |it, method|
      method = [method] unless method.is_a? Array
      Enumeravle::Enumerator.new( it, *method ) 
    }.each( &Proc.new )
  end

  #
  #
  #
  def to_hash
    result = {}
    if block_given? then
      each { |value|
        key, val, = yield value
        result[key] = val
      }
    else
      each { |value|
        result[value[0]] = value[1]
      }
    end
    result
  end
end


class Array
  # 
  # 参照: http://d.hatena.ne.jp/sesejun/20070502/p1
  # ライセンス: GPL2
  # 

  #
  # ここから
  #

  # 
  # 内部関数。
  # [合計, 長さ] を返します。
  # 
  def sum_with_number
    total = 0.0
    len = 0
    self.each do |v|
      next if v.nil?
      total += v.to_f
      len += 1
    end
    [ total, len ]
  end

  # 
  # 合計を返します。
  # 
  def sum
    total, len = self.sum_with_number
    total
  end

  # 
  # 平均を返します。
  # 
  def avg
    total, len = self.sum_with_number
    total / len
  end
  alias mean avg

  # 
  # 分散を返します。
  # 
  def var
    c = 0
    while self[c].nil do
      c += 1
    end
    mean = self[c].to_f
    sum = 0.0
    n = 1
    (c + 1).upto( self.size - 1 ) do |i|
      next if self[i].nil?
      sweep = n.to_f / ( n + 1.0 )
      delta = self[i].to_f - mean
      sum += delta * delta * sweep
      mean += delta / (n + 1.0)
      n += 1
    end
    sum / n.to_f
  end

  # 
  # 標準偏差を返します。
  # 
  def stddev
    Math.sqrt self.var
  end

  # 
  # (a[0], b[0]), (a[1], b(1)),... の相関係数を返します。
  # 
  def corrcoef(y)
    raise "Invalid Argument Array Size" unless self.size == y.size
    sum_sq_x = 0.0
    sum_sq_y = 0.0
    sum_coproduct = 0.0
    c = 0
    while self[c].nil? || y[c].nil? do
      c += 1
    end
    mean_x = self[c].to_f
    mean_y = y[c].to_f
    n = 1
    (c + 1).upto( self.size - 1 ) do |i|
      next if self[i].nil? || y[i].nil?
      sweep = n.to_f / (n + 1.0)
      delta_x = self[i].to_f - mean_x
      delta_y = y[i].to_f - mean_y
      sum_sq_x += delta_x * delta_x * sweep
      sum_sq_y += delta_y * delta_y * sweep
      sum_coproduct += delta_x * delta_y * sweep
      mean_x += delta_x / (n + 1.0)
      mean_y += delta_y / (n + 1.0)
      n += 1
    end
    pop_sd_x = Math.sqrt sum_sq_x / n.to_f
    pop_sd_y = Math.sqrt sum_sq_y / n.to_f
    cov_x_y = sum_coproduct / n.to_f
    cov_x_y / ( pop_sd_x * pop_sd_x )
  end

  #
  # ここまで
  #
  include Comparable

  # 
  # _index_ 番目から _length_ 個の要素を先頭に持っていきます。
  # 
  def bubble_up!(index, length = 1)
    if index.abs >= self.size then
      return nil
    end
    self[0, 0] = self.slice! index, length
    self
  end

  def bubble_up(index, length = 1)
    self.clone.bubble_up! index, length
  end

  # 
  # _index_ 番目から _length_ 個の要素を末尾に持っていきます。
  #  
  def bubble_down!(index, length = 1)
    if index.abs >= self.size then
      return nil
    end
    self[(self.size - length)..0] = self.slice! index, length
    self
  end

  def bubble_down(index, length = 1)
    self.clone.bubble_down! index, length
  end

  if RUBY_VERSION < "1.9" then
    # 
    # 1.9 の rindex と同じ挙動です。
    # 
    def reverse_index(val = nil, &proc)
      if proc then
        val = (Class.new { define_method( :==, &proc ) }).new
      end
      rindex val
    end

    # 
    # 1.9 の index と同じようにブロックを渡すことができます。
    # 
    def index(val = nil)
      compare = if block_given?
                  Proc.new
                else
                  lambda { |x| x == val }
                end
      each_with_index { |x, i|
        return i if compare[x]
      }
      nil
    end
  end

  def symbolize
    result = []
    each { |val|
      result << if val.respond?to :symbolize then  val.symbolize else val end
    }
  end
end


class Hash
  # 
  # キーの名前を変換します。
  # 
  def convert_key(rule = nil)
    if block_given? then
      convert_key_proc &Proc.new
    else
      convert_key_hash rule
    end
  end

  #
  #
  #
  def convert_key_proc(&rule)
    result = {}
    self.each_pair { |key, val|
      result[rule.call( key )] = val
    }
    result
  end

  #
  #
  #
  def convert_key_hash(rule)
    result = {}
    self.each_pair { |key, val|
      if rule[key] then
        result[rule[key]] = val
      else
        result[key.to_sym] = val
      end
    }
    result
  end

  # 
  # キーを全て to_sym した hash を新しく作成します。
  # 
  def symbolize
    result = {}
    each_pair { |key, val|
      result[key.to_sym] = if val.respond_to? :symoblize then val.symbolize else val end
    }
    result
  end
end
